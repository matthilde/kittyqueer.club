CREATE TABLE schema_migrations (version text primary key)
CREATE TABLE entry (
  id integer primary key,
  site text unique not null,
  owner text not null,
  description text,
  tags text,
  created_at integer not null default(strftime('%s', 'now')),
  updated_at integer
)