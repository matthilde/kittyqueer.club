-- up
create table entry (
  id integer primary key,
  site text unique not null,
  owner text not null,
  description text,
  tags text,
  created_at integer not null default(strftime('%s', 'now')),
  updated_at integer
)

-- down
drop table entry