(use joy)
(use ./routes/entry)

# File to string
(defn f->string [filename]
  (with [f (file/open filename)]
    (file/read f :all)))

(def ringmeta
  {:name        "the kittyqueer club"
   :description "webring for queer kitties uwu"
   :domain      "https://kittyqueer.club"
   :about       (string (f->string "mainpage.txt"))})

# Layout
(defn app-layout [{:body body :request request}]
  (text/html
    (doctype :html)
    [:html {:lang "en"}
     [:head
      [:title (ringmeta :name)]
      [:meta {:charset "utf-8"}]
      [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
      [:link {:href "https://cdn.simplecss.org/simple.min.css" :rel "stylesheet"}]
      [:link {:href "/app.css" :rel "stylesheet"}]]
     [:body
      [:header {:class "tc"}
        
        [:h1 
         [:img {:src "/blobcat.png" :style "width: 64px; margin-right: 16px"}]
         [:span (ringmeta :name)]]
        [:p [:i (ringmeta :description)]]
        [:nav
         [:a {:href "/"}       "Home"]
         [:a {:href "/search"} "Search websites"]
         [:a {:href "/about"}  "About the webring..."]
        ]]
      [:main
       body]
      [:footer
       "Powered by " [:a {:href "https://joyframework.com/"} "Joy"]
       " and written in Janet by " 
       [:a {:href "https://cutebunni.es"} "Matthilde"] "."]]]))
#
# Some needed functions
#

# Random integer
(defn randint [mx]
  (math/floor (* (math/random) mx)))

# Shuffle a list randomly
(defn shuffle [xs]
  (def arr (array/new (length xs)))
  (each x xs
    (array/insert arr (randint (length arr)) x))
  arr)

# Does it contains item?
(defn contains [arr x]
  (not (nil? (index-of x arr))))

# Safe slice
(defn safeslice [arr end]
  (if (< end (length arr))
    (array/slice arr 0 end) arr))

# Security stuff to verify if the tag list is ok
(defn tags-verify [x]
  (string/check-set "abcdefghijklmnopqrstuvwxyz, " x))

# Parse tags
(defn parse-tags [tags]
  (if (tags-verify tags)
    (map string/trim (string/split "," tags))
    []))

# Check if tags matches
(defn tags-match [ind pat]
  (let 
  [meep 
   (all truthy? (map (fn [x] (contains ind x)) pat))
  ]
  
  (printf "%p" meep)
  meep))

#
# DB functions
#

# Get all entries
(defn entries []
  (db/from :entry))
# Generate HTML from entries
(defn entries->html [xs]
  (def res (array/new (+ (length xs) 1)))
  (array/push res :section)
  (each x xs
    (array/push res
      [:article
       [:a {:href (x :site)} [:h2 (x :site)]]
       [:p [:b "by "] [:i (x :owner)] [:b " - tags: "] [:i (x :tags)] [:br]
       [:span (x :description)]]
       ]))
  res)

#
# Search functions
#

# Search by tags
(defn filter-tags [tags]
  (filter
    (fn [x] (tags-match (parse-tags (x :tags)) tags))
    (entries)))

#
# Routes
#
(route :get "/" :home)
(route :get "/about" :about)
(route :get "/search" :whole-index)
(route :get "/search/:tags" :search)

(defn whole-index [request]
  [:div
   [:p "To search with tags, change the url like this: "
    [:code (string (ringmeta :domain) "/search/tag1,tag2")]]
   (entries->html (entries))
  ])

(defn search [request]
  (let [tags (parse-tags (get-in request [:params :tags]))]
    (entries->html (shuffle (filter-tags tags)))))

(defn home [request]
  [:div
   [:p (string "Welcome to " (ringmeta :name) "! This is a webring containing
                multiple
                sites made by queer people. Feel free to look around! Wanna add
                your site? Check the 'about' section for the ToS!")]
   [:p "This page picks 5 random sites from the database and put them here. Each
        time you reload this page, the results changes. You can use the search
        engine if you want to find something more specific."]
   [:p "If you are a member, you can add this badge on your website if you want
        to!" [:img {:src "/badge.png"}] [:span " using this embed..."]]
   [:pre
      `<a href="https://kittyqueer.club"><img src="https://kittyqueer.club/badge.png" /></a>` ]

   (entries->html 
     (safeslice (shuffle (entries)) 5))])

(defn about [request]
  [:div
    [:pre {:style "font-size: 70%"} (ringmeta :about)]
    [:p [:b (ringmeta :name)][:br]
    [:i (ringmeta :description)]]
    [:p
     "Webring back-end written in Janet by Matthilde" [:br]
     [:a {:href "https://joyframework.com/"}
      "Powered by Joy Web Framework"] [:br]
     [:a {:href "https://simplecss.org"} "Using simple.css"]]
    [:p "This project is Free and Open Source Software and is hosted in Codeberg."
      [:a {:href "https://codeberg.org/matthilde/kittyqueer.club"} "Git repo"]]])

# Search engine

# Middleware
(def app (-> (handler)
             (layout app-layout)
             (extra-methods)
             (query-string)
             (body-parser)
             (json-body-parser)
             (server-error)
             (x-headers)
             (static-files)
             (not-found)
             (logger)))


# Server
(defn main [& args]
  (db/connect (env :database-url))
  (server app (env :port))
  (db/disconnect))
