(use joy)

# THIS CODE IS NOT PASSWORD-PROTECTED ONLINE
# PLEASE USE HTTP BASIC AUTH ON /entries

(route :get "/entries" :entries/index)
(route :get "/entries/new" :entries/new)
(route :get "/entries/:id" :entries/show)
(route :post "/entries" :entries/create)
(route :get "/entries/:id/edit" :entries/edit)
(route :patch "/entries/:id" :entries/patch)
(route :delete "/entries/:id" :entries/delete)


(defn entry [req]
  (let [id (get-in req [:params :id])]
    (db/find :entry id)))


(def entry/body
  (body :entry
    (validates [ :site :owner :description :tags] :required true)
    (permit [ :site :owner :description :tags])))


(defn entries/index [req]
  (let [entries (db/from :entry)]
   [:div
    [:a {:href (url-for :entries/new)} "New entry"]

    [:table
     [:thead
      [:tr
       [:th "id"]
       [:th "site"]
       [:th "owner"]
       [:th "description"]
       [:th "tags"]
       [:th "created-at"]
       [:th "updated-at"]
       [:th]
       [:th]
       [:th]]]
     [:tbody
      (foreach [entry entries]
        [:tr
          [:td (entry :id)]
          [:td (entry :site)]
          [:td (entry :owner)]
          [:td (entry :description)]
          [:td (entry :tags)]
          [:td (entry :created-at)]
          [:td (entry :updated-at)]
          [:td
           [:a {:href (url-for :entries/show entry)} "Show"]]
          [:td
           [:a {:href (url-for :entries/edit entry)} "Edit"]]
          [:td
           (form-for [req :entries/delete entry]
            [:input {:type "submit" :value "Delete"}])]])]]]))


(defn entries/show [req]
  (when-let [entry (entry req)]

    [:div
     [:strong "id"]
     [:div (entry :id)]

     [:strong "site"]
     [:div (entry :site)]

     [:strong "owner"]
     [:div (entry :owner)]

     [:strong "description"]
     [:div (entry :description)]

     [:strong "tags"]
     [:div (entry :tags)]

     [:strong "created-at"]
     [:div (entry :created-at)]

     [:strong "updated-at"]
     [:div (entry :updated-at)]


     [:a {:href (url-for :entries/index)} "All entries"]]))


(defn entries/new [req &opt errors]
  (let [entry (entry/body req)]

    (form-for [req :entries/create]
      [:div
       [:label {:for "site"} "site"]
       [:input {:type "text" :name "site" :value (entry :site)}]
       [:small (get errors :site)]]

      [:div
       [:label {:for "owner"} "owner"]
       [:input {:type "text" :name "owner" :value (entry :owner)}]
       [:small (get errors :owner)]]

      [:div
       [:label {:for "description"} "description"]
       [:input {:type "text" :name "description" :value (entry :description)}]
       [:small (get errors :description)]]

      [:div
       [:label {:for "tags"} "tags"]
       [:input {:type "text" :name "tags" :value (entry :tags)}]
       [:small (get errors :tags)]]

      [:input {:type "submit" :value "Save"}])))


(defn entries/create [req]
  (let [entry (-> req entry/body db/save)]

    (if (saved? entry)
      (redirect-to :entries/index)
      (entries/new req (errors entry)))))


(defn entries/edit [req &opt errors]
  (when-let [entry (entry req)
             entry (merge entry (entry/body req))]

    (form-for [req :entries/patch entry]
      [:div
       [:label {:for "site"} "site"]
       [:input {:type "text" :name "site" :value (entry :site)}]
       [:small (get errors :site)]]

      [:div
       [:label {:for "owner"} "owner"]
       [:input {:type "text" :name "owner" :value (entry :owner)}]
       [:small (get errors :owner)]]

      [:div
       [:label {:for "description"} "description"]
       [:input {:type "text" :name "description" :value (entry :description)}]
       [:small (get errors :description)]]

      [:div
       [:label {:for "tags"} "tags"]
       [:input {:type "text" :name "tags" :value (entry :tags)}]
       [:small (get errors :tags)]]

      [:input {:type "submit" :value "Save"}])))


(defn entries/patch [req]
  (when-let [entry (entry req)
             entry (->> req entry/body (merge entry) db/save)]

    (if (saved? entry)
      (redirect-to :entries/index)
      (entries/edit req (errors entry)))))


(defn entries/delete [req]
  (when-let [entry (entry req)]

    (db/delete entry)

    (redirect-to :entries/index)))
